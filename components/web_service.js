// -----------------------------------------------------------------------------------------------
// Leemos el archivo de config .json
// -----------------------------------------------------------------------------------------------
const configFileName = "_web_service.json"
const fs = require("fs"); 
const { Console } = require("console");
fs.readFile(configFileName, function(err, data) 
{ 
    // Chequeamos por si nos devuelve el error 
    if (err) throw err; 
    // Convertimos el contenido del archivo en JSON 
    const config = JSON.parse(data);
    // -----------------------------------------------------------------------------------------------
    // Nos conectamos a la base de datos con los datos que leimos del archivo
    // -----------------------------------------------------------------------------------------------
    console.log(config);
    var mysql = require('mysql');
    var conn = mysql.createConnection(
    {
        host     : config.host,
        user     : config.user,
        password : config.password,
        database : config.database
    }
    );
    conn.connect(function (err) {
        if (err) throw err;
        console.log('se conecto exitosamente a la base de datos');
        // -----------------------------------------------------------------------------------------------
        // Empezamos a escuchar las conexiones en el puerto 10000
        // -----------------------------------------------------------------------------------------------
        app.listen(10000, function(err) {
            if (err) throw err;
            console.log("Web server esta escuchando en puerto 10000");
          });
     });
    // -----------------------------------------------------------------------------------------------
    // Server http
    // -----------------------------------------------------------------------------------------------
    var express = require('express');
    var cors = require('cors');
    var bodyParser = require('body-parser');
    var app = express();
    app.use(bodyParser.json());
    app.options('*', cors());
    app.use(cors());

    app.get('/', function(request, response) 
    {
        response.send(' -------------- API de Medline v0.1 -----------------');
    });
    // -----------------------------------------------------------------------------------------------
    // Metodo GET que ejecuta un consulta que trae todas las sintomas (id,descripcion)
    // -----------------------------------------------------------------------------------------------
    app.get('/sintomas', function(request, response) 
    {
        conn.query('select * from sintomas', function (err, result) 
        {
            if (err) throw err;
            response.send(result);
        }); 
    });
        // -----------------------------------------------------------------------------------------------
    // Metodo GET que ejecuta un consulta que trae todas las coberturas (id,descripcion)
    // -----------------------------------------------------------------------------------------------
    app.get('/coberturas', function(request, response) 
    {
        conn.query('select * from coberturas', function (err, result) 
        {
            if (err) throw err;
            response.send(result);
        }); 
    });
    // -----------------------------------------------------------------------------------------------
    // Metodo POST que ejecuta un consulta recibiendo un arreglo que contiene: el nombre del usuario, contraseña
    // y si el usurario y su respectiva contraseña pertencen a algun paciente devuelve 
    // nombre,apellido,id de la cobertura,coordenandas de usuario,descripcion de la cobertura y si no devuelve una cadena vacia.
    // -----------------------------------------------------------------------------------------------
    app.post('/loginPaciente', function(request, response)
    {
        var credential = request.body;
        if (!credential.user || !credential.pass) {
            return response.send("El usuario y password del usuario no puede ser vacio");
          };
        var sql = 'select p.nombre, p.apellido, p.id_cobertura, p.coordenadas_latitud, p.coordenadas_longitud, c.descripcion FROM pacientes p'
                + ' join coberturas c on (p.id_cobertura = c.id_cobertura)'
                + ' where p.nombre_usuario = ? AND p.contraseña = ? limit 1;';
        conn.query(sql,[credential.user,credential.pass],function(err, result)
        {
            if (err) throw err;
            response.send(result);
        }); 
    });
    // -----------------------------------------------------------------------------------------------
    // Metodo POST que crea el regisrto del paciente en la tabla pacientes
    // metodo recibe (user,pass,id_cobertura,nombre,apellido,coordenadas_latitud,coordenadas_longitud)
    // !!! user y pass no pueden ser nullas !!!!!!
    // En caso de el nombre del usuario ya existe 
    // -----------------------------------------------------------------------------------------------
    app.post('/crearPaciente', function(request, response)
    {
        var paciente = request.body;
        if (!paciente.user || !paciente.pass) {
          return response.send("El nombre del usuario y contraseña del usuario no puede ser vacio");
        }  
        var pacienteArray = [
          paciente.user,
          paciente.pass,
          paciente.email,
          paciente.id_cobertura,
          paciente.nombre,
          paciente.apellido,
          paciente.coordenadas_latitud,
          paciente.coordenadas_longitud
        ]; 
        // -----------------------------------------------------------------------------------------------
        // Consulta que busca coincidencia del nombre del usuario nuevo en la tabla de pacientes
        // -----------------------------------------------------------------------------------------------
        conn.query('select id_paciente from pacientes where nombre_usuario = ?',[paciente.user],function (err, result) 
        {
            if (err) throw err;
            if(!result[0])
                {  
                    // -----------------------------------------------------------------------------------------------
                    // Consulta que trae todos los usuarios
                    // -----------------------------------------------------------------------------------------------
                    conn.query('insert into pacientes (nombre_usuario, contraseña, email, id_cobertura, nombre, apellido,  coordenadas_latitud, coordenadas_longitud)'
                    +' values (?, ?, ?, ?, ?, ?, ?, ?) ', pacienteArray, function(err, result) 
                        {
                        if (err) throw err;
                        response.send("Se ha creado registro del nuevo paciente id:"+ result.insertId);
                    });
                }
            else response.send("Ya nombre de usuario : " + paciente.user + " ya existe !!!!");
        });
      });
    // -----------------------------------------------------------------------------------------------
    // Metodo GET que ejecuta una consulta recibiendo un arreglo que contiene :id de sintoma,
    // coordenandas de usuario,distancia maxima de busqueda y id de cobertura.
    // Metodo devuelve el listado de los especialistas que atienden por su sintomas,cobertura medica
    // y sus consultorios se encuentran dentro del radio de busqueda (distancia maxima)
    // -----------------------------------------------------------------------------------------------
    app.get('/buscarEspecialista/:sintomas/:coordenadas_usuario/:distancia_maxima/:id_cobertura', function(request, response) 
    {
        var sintomas             = (request.params.sintomas).split(",");
        var coordenadas_usuario  = (request.params.coordenadas_usuario).split(",");
        var distancia_maxima     = parseFloat((request.params.distancia_maxima));
        var id_cobertura         = parseInt((request.params.id_cobertura));
        if(!sintomas) return response.send('debe al menos un sintoma');
        var sql = ' select  p.id_patologia as id_patologia , p.descripcion as patologia, SUM(sp.scoring) as puntaje_sumado , p.scoring_requerido as puntaje_requerido'
                + ' from sintoma_patologia sp'
                + ' join patologias p on (p.id_patologia = sp.id_patologia)'
                + ' where id_sintoma IN (?)'
                + ' group by sp.id_patologia'
                + ' having puntaje_sumado > puntaje_requerido'
                + ' order by puntaje_sumado desc limit 1;';
        // -----------------------------------------------------------------------------------------------
        // Consulta que trae la patologia que corresponde a los sintomas
        // -----------------------------------------------------------------------------------------------
        conn.query(sql,[sintomas], function (err, result) 
        {
            if (err) throw err;
            if(result[0])
                {
                    var sql = ' select es.descripcion,e.nombre,e.email,e.telefono,e.domicilio_consultorio,e.localidad,e.coordenadas_latitud,e.coordenadas_longitud'
                            + ' from especialistas e'
                            + ' join patologia_especialidad pe on (e.id_especialidad = pe.id_especialidad)'
                            + ' join especialidades es on (es.id_especialidad = pe.id_especialidad)'
                            + ' where  pe.id_patologia = ? and e.id_cobertura = ? limit 100;';
                    // -----------------------------------------------------------------------------------------------
                    // Consulta que trae los epecialistas
                    // -----------------------------------------------------------------------------------------------
                    conn.query(sql,[result[0].id_patologia,id_cobertura], function (err, result) 
                    {
                        if (err) throw err;
                        var especialistas = [];
                        for (var especialista of result)
                            {
                                var p = 0.017453292519943295;    // Math.PI / 180
                                var c = Math.cos;
                                var a = 0.5 - c((especialista.coordenadas_latitud - coordenadas_usuario[0]) * p)/2 + 
                                        c(coordenadas_usuario[0] * p) * c(especialista.coordenadas_latitud * p) * 
                                        (1 - c((especialista.coordenadas_longitud - coordenadas_usuario[1]) * p))/2;
                                var dist = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
                                if (dist <= distancia_maxima)
                                {
                                    var e = { 
                                        "nombre"               :''
                                      , "especialidad"         :''
                                      , "domicilio_consultorio":''
                                      , "localidad"            :''
                                      , "telefono"             :''
                                      , "email"                :''
                                      , "distancia"            :0
                                     };
                                    e.nombre = especialista.nombre;
                                    e.especialidad = especialista.descripcion;
                                    e.domicilio_consultorio = especialista.domicilio_consultorio;
                                    e.localidad = especialista.localidad;
                                    e.telefono = especialista.telefono;
                                    e.email = especialista.email;
                                    e.distancia = dist;
                                    especialistas.push(e);
                                };
                            }
                        especialistas.sort(function (x, y) {
                            return x.distancia - y.distancia;
                        });
                        response.send(especialistas);
                    });
                }
            else response.send("No se ha podido diagnosticar su patologia!!!!"); 
        }); 
    });
}); 
